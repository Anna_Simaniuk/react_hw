import {Component, ReactNode} from 'react';
import s from './App.module.scss';
import Modal from "./Components/Modal/Modal";
import Button from "./Components/Button/Button";


interface AppState {
    openModal: boolean;
    header: string;
    closeButton: boolean,
    text: string,
    actions: ReactNode
}

class App extends Component <any, AppState> {
    state: AppState = {
        openModal: false,
        header: '',
        closeButton: false,
        text: '',
        actions: <></>
    }

    openFirstModal = () => {
        const text:string = 'Once you delete this file, it won\'t be possible to undo this action. Are you sure you want to delete it?'
        const actions:ReactNode = <>
            <Button text={'OK'} backgroundColor={'#ff9a50'} onClick={this.closeModal}/>
            <Button text={'Cancel'} backgroundColor={'#ff9a50'} onClick={this.closeModal}/>
        </>

        this.setState({openModal:true});
        this.setState({header:'Do you want to delete this file?'});
        this.setState({closeButton: true});
        this.setState({text: text});
        this.setState({actions: actions})
    }

    openSecondModal = () => {
        const text:string = 'Some products...'
        const actions:ReactNode = <>
            <Button text={'OK'} backgroundColor={'rgb(172 255 241)'} onClick={this.closeModal}/>
            <Button text={'Cancel'} backgroundColor={'rgb(172 255 241)'} onClick={this.closeModal}/>
        </>

        this.setState({openModal:true});
        this.setState({header:'Do you want to buy this product?'});
        this.setState({closeButton: true});
        this.setState({text: text});
        this.setState({actions: actions})
    }

    closeModal = () =>{this.setState({openModal:false})}

    render() {
        const {openModal, header, closeButton, text, actions} = this.state;

        return <div className={s.app}>
            <Button
                backgroundColor='#fff'
                text='First Modal'
                onClick={this.openFirstModal}/>
            <Button
                backgroundColor='#d5d5d5'
                text='Second Modal'
                onClick={this.openSecondModal}/>

            <Modal
                header={header}
                closeButton={closeButton}
                text={text}
                actions={actions}
                openModal={openModal}
                closeModal={this.closeModal}/>
        </div>
    }
}

export default App;
