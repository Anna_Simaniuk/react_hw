import {Component, MouseEventHandler, ReactNode} from "react";
import s from "./Modal.module.scss"

interface ModalProps {
    header: string;
    closeButton: boolean;
    text: string;
    actions: ReactNode;
    openModal: boolean;
    closeModal: MouseEventHandler<HTMLElement>;
}

export default class Modal extends Component <ModalProps> {
    render() {
        const {openModal, closeModal, header, closeButton, text, actions} = this.props

        return (
            <> {openModal &&
                (<div className={s.modal}>
                    <div className={s.overlay} onClick={closeModal} />
                        <div className={s.modalContentWrapper}>
                            {closeButton && <button className={s.modalClose} onClick={closeModal}>✗</button>}
                            <h3 className={s.modalHeader}>{header}</h3>
                            <div className={s.modalText}>{text}</div>
                            <div className={s.modalActions}>{actions}</div>
                        </div>
                </div>
                )}
            </>
        );
    }
}