import {Component, MouseEventHandler} from "react";
import s from "./Button.module.scss"

interface ButtonProps {
    text: string;
    backgroundColor: string;
    onClick: MouseEventHandler<HTMLButtonElement>;
}

export default class Button extends Component <ButtonProps> {
    render() {
        const { backgroundColor, onClick, text } = this.props

        return <button
            style={{backgroundColor: backgroundColor}}
            className={s.button}
            onClick={onClick}
        >{text}</button>;
    }
}