import React, {Component} from 'react';
import s from './App.module.scss';
import Navbar from "./pages/Navbar/Navbar";
import Products from "./pages/Products/Products";


interface AppState {
    countHearts: number;
    countBaskets: number;
}

class App extends Component <any, AppState> {
    state: AppState = {
        countHearts: 0,
        countBaskets: 0,
    }

    componentDidMount() {
        this.setState({countHearts: Number(localStorage.getItem('countHearts'))})
        this.setState({countBaskets: Number(localStorage.getItem('countBaskets'))})
    }

    counterHeardIncrement = () => {
        this.setState({countHearts: this.state.countHearts + 1})
    }
    counterHeardDecrement = () => {
        this.setState({countHearts: this.state.countHearts - 1})
    }
    counterBasketIncrement = () => {
        this.setState({countBaskets: this.state.countBaskets + 1})
    }
    counterBasketDecrement = () => {
        this.setState({countBaskets: this.state.countBaskets - 1})
    }

    componentDidUpdate() {
        localStorage.setItem('countHearts', this.state.countHearts.toString())
        localStorage.setItem('countBaskets', this.state.countBaskets.toString())
    }

    render() {
        const {countHearts, countBaskets} = this.state;

        return (
            <div className={s.app}>
                <Navbar counterHeard={countHearts} counterBasket={countBaskets}/>
                <Products
                    counterHeardIncrement={this.counterHeardIncrement}
                    counterHeardDecrement={this.counterHeardDecrement}
                    counterBasketIncrement={this.counterBasketIncrement}
                    counterBasketDecrement={this.counterBasketDecrement}/>
            </div>
        );
    }
}

export default App;