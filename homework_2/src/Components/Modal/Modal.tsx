import {Component, MouseEventHandler} from "react";
import s from './Modal.module.scss';
import Button from "../Button/Button";

interface ModalProps {
    header: string;
    closeModal: MouseEventHandler<HTMLElement>;
    url: string;
    productContent: string;
    productPrice: string;
}

export default class Modal extends Component <ModalProps> {
    render() {
        const {closeModal, header, url, productContent, productPrice} = this.props

        return (
            <div className={s.modal}>
                <div className={s.overlay} onClick={closeModal}/>
                <div className={s.modalContentWrapper}>
                    <button className={s.modalClose} onClick={closeModal}>✖</button>
                    <h3 className={s.modalHeader}>{header}</h3>
                    <div className={s.productContent}>
                        <img className={s.productImg} src={url} alt="product"/>
                        <p className={s.productContent}>{productContent}</p>
                        <p className={s.productPrice}>{productPrice}</p>
                    </div>
                    <Button classBtn={s.modalBtn} onClick={closeModal}>Продовжити покупки</Button>
                </div>
            </div>
        );
    }
}