import {Component, ReactNode, MouseEventHandler} from "react";
import s from './Button.module.scss';
import cn from 'classnames';

interface ButtonProps {
    children: ReactNode;
    classBtn?: string;
    counter?: number;
    onClick: MouseEventHandler<HTMLButtonElement>;
    backgroundColorCounter?: string
}

export default class Button extends Component<ButtonProps> {
    render() {
        const {children, classBtn = '', counter = 0, onClick, backgroundColorCounter } = this.props

        return <div className={s.buttonWrapper}>
            <button
                className={cn(s.button, classBtn)}
                onClick={onClick}>{children}</button>
            {(counter > 0) && <span style={{backgroundColor: backgroundColorCounter}} className={s.counterBtn}>{counter}</span>}
        </div>
    }
}