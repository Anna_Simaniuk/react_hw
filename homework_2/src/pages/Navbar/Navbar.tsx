import {Component} from "react";
import logo from '../../assets/img/logo.svg'
import s from './Navbar.module.scss'
import Button from "../../Components/Button/Button";
import heart from '../../assets/img/heartWhite.png';
import basket from '../../assets/img/basketWhite.png'


interface NavbarProps{
    counterHeard: number;
    counterBasket: number;
}

export default class Navbar extends Component<NavbarProps> {

    dropDownHandler = () => {
        console.log('dropdown')
    }

    render() {
       const {counterHeard, counterBasket} = this.props;

        return (
            <nav className={s.navbar}>
                <div><img src={logo} alt="logo"/></div>
                <div className={s.navBtnWrapper}>
                    <Button
                        counter={counterHeard}
                        onClick={this.dropDownHandler}>
                        <img src={heart} alt="heart"/>
                    </Button>
                    <Button
                        counter={counterBasket}
                        backgroundColorCounter={'#00a046'}
                        onClick={this.dropDownHandler}>
                        <img src={basket} alt="basket"/>
                    </Button>
                </div>
            </nav>
        );
    }
}