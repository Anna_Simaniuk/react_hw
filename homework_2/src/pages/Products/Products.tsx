import React, {Component} from "react";
import s from './Products.module.scss'
import Product from "../Product/Product";

interface ProductsProps {
    counterHeardIncrement: React.ComponentState;
    counterHeardDecrement: React.ComponentState;
    counterBasketIncrement: React.ComponentState;
    counterBasketDecrement: React.ComponentState;
}

interface ProductsState {
    products: object[];
    favouriteProduct: object[];
}

export default class Products extends Component<ProductsProps, ProductsState> {
    state: ProductsState = {
        products: [],
        favouriteProduct: [],
    }

    async componentDidMount() {
        try {
            const request = await fetch('./data.json')
            const res = await request.json();
            this.setState({products: res})
        } catch (e) {
            console.log((e as Error).message)
        }
    }

    counterHeardIncrement = () => {
        this.props.counterHeardIncrement()
    }
    counterHeardDecrement = () => {
        this.props.counterHeardDecrement()
    }
    counterBasketIncrement = () => {
        this.props.counterBasketIncrement()
    }
    counterBasketDecrement = () => {
        this.props.counterBasketDecrement()
    }

    renderProductTable(card: object): JSX.Element {
        const {product, price, url, article, color}: { [key: string]: string | any } = card;

        return <Product key={article}
                        url={url}
                        roundColor={color}
                        productName={product}
                        price={price}
                        counterHeardIncrement={this.counterHeardIncrement}
                        counterHeardDecrement={this.counterHeardDecrement}
                        counterBasketDecrement={this.counterBasketDecrement}
                        counterBasketIncrement={this.counterBasketIncrement}
                        article={article}/>
    }

    render() {
        const {products} = this.state

        return <div className={s.products}>
            {products.length > 0 && products.map((product: object) => this.renderProductTable(product))}
        </div>;
    }
}