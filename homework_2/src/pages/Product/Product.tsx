import React, {Component} from "react";
import s from './Product.module.scss';
import Button from "../../Components/Button/Button";
import heardFill from '../../assets/img/heartFill.png';
import heardOrange from '../../assets/img/heartOrange.png';
import basketGreen from '../../assets/img/basketGreen.png';
import basketFill from '../../assets/img/basketFill.png';
import Modal from "../../Components/Modal/Modal";

interface ProductProps {
    url: string;
    roundColor: string;
    productName: string;
    price: string;
    article: string;
    counterHeardIncrement: React.ComponentState;
    counterHeardDecrement: React.ComponentState;
    counterBasketIncrement: React.ComponentState;
    counterBasketDecrement: React.ComponentState;
}

interface ProductState {
    statusBasket: boolean;
    statusHeart: boolean;
    statusModal: boolean;
}

export default class Product extends Component<ProductProps, ProductState> {
    state: ProductState = {
        statusBasket: false,
        statusHeart: false,
        statusModal: false,
    }

    handleClickHeard = () => {
        const {statusHeart} = this.state;
        const {counterHeardDecrement, counterHeardIncrement, article} = this.props;

        if (statusHeart) {
            this.setState({statusHeart: false})
            counterHeardDecrement()
            localStorage.removeItem(`statusHeart${article}`)
        } else {
            this.setState({statusHeart: true})
            counterHeardIncrement()
            localStorage.setItem(`statusHeart${article}`, 'true')
        }
    }

    handleClickBasket = () => {
        const {statusBasket} = this.state;

        if (statusBasket) {
            this.setState({statusBasket: false})
            this.props.counterBasketDecrement()
            localStorage.removeItem(`statusBasket${this.props.article}`)
        } else {
            document.body.style.overflowY = 'hidden'
            this.setState({statusBasket: true, statusModal: true})
            this.props.counterBasketIncrement()
            localStorage.setItem(`statusBasket${this.props.article}`, 'true')
        }
    }

    closeModal = () => {
        document.body.style.overflowY = 'auto'
        this.setState({statusModal: false})
    }


    componentDidMount() {
        if (!!localStorage.getItem(`statusHeart${this.props.article}`)) {
            this.setState({statusHeart: true})
        }
        if (!!localStorage.getItem(`statusBasket${this.props.article}`)) {
            this.setState({statusBasket: true})
        }
    }

    render() {
        const {url, roundColor, productName, price} = this.props;
        const {statusHeart, statusBasket, statusModal} = this.state;

        return <>
            <div className={s.product}>
                <Button onClick={this.handleClickHeard} classBtn={s.buttonHeard}>
                    <img src={statusHeart ? heardFill : heardOrange} alt="heard"/>
                </Button>
                <img className={s.img} src={url} alt="product"/>
                <span style={{backgroundColor: roundColor}} className={s.round}></span>
                <p className={s.productName}>{productName}</p>
                <div className={s.priceWrapper}>
                    <p className={s.price}>{price}</p>
                    <Button onClick={this.handleClickBasket}>
                        <img src={statusBasket ? basketFill : basketGreen} alt="heard"/>
                    </Button>
                </div>
            </div>

            {statusModal && <Modal
                header={'Ваш товар успішно додано в кошик'}
                closeModal={this.closeModal}
                url={url}
                productContent={productName}
                productPrice={price}/>}
        </>
    }
}